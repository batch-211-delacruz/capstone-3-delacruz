import Banner from '../components/Banner';
import Featured from '../components/Highlights';
import Products from '../pages/Products';

export default function Home(){

	const data = {
		title: "Codiex Tech Stuff",
		content: "Welcome! Buy the latest and greatest tech products today",
		destination: "/products",
		label: "Shop now!"
	}

	return(
		<>
			<Banner bannerProp={data}/>
			<Featured/>
			<Products/>
		</>
	)
}