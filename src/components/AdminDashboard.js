import { useContext, useState, useEffect } from "react";
import {Table, Button} from "react-bootstrap";
import {Navigate, Link} from "react-router-dom";
import UserContext from "../UserContext";
import Swal from "sweetalert2";

export default function AdminDash(){

	const {user} = useContext(UserContext);

	const [allProducts, setProducts] = useState([]);

	const getData = () =>{
		fetch(`https://e-commerce-api-djx5.onrender.com/products/all`,{
			headers:{
				"Authorization": `Bearer ${localStorage.getItem("token")}`
			}
		})
		.then(res => res.json())
		.then(data => {
		

			setProducts(data.map(product => {
				return(
					<tr key={product._id}>
						<td>{product.name}</td>
						<td>{product.description}</td>
						<td>Php: {product.price}</td>
						<td>{product.isActive ? "Active" : "Inactive"}</td>
						<td>
							{
							
								(product.isActive)
								?	
								 	
									<Button variant="danger" size="sm" onClick ={() => deactivate(product._id, product.name)}>Set as Inactive</Button>
								:
									<>
										
										<Button variant="success" size="sm" onClick ={() => activate(product._id, product.name)}>Set as Active</Button>
										
										<Button as={ Link } to={`/editProduct/${product._id}`} variant="secondary" size="sm" className="m-2" >Edit product</Button>
									</>
							}
						</td>
					</tr>
				)
			}))

		})
	}

	
	const deactivate = (productId, productName) =>{


		fetch(`https://e-commerce-api-djx5.onrender.com/products/${productId}/archive`,{
			method: "PUT",
			headers:{
				"Content-Type": "application/json",
				"Authorization": `Bearer ${localStorage.getItem('token')}`
			},
			body: JSON.stringify({
				isActive: false
			})
		})
		.then(res => res.json())
		.then(data =>{
	

			if(data){
				Swal.fire({
					title: "Deactivate Succesful!",
					icon: "success",
					text: `${productName} is now inactive.`
				})
				getData();
			}
			else{
				Swal.fire({
					title: "Deactivate Unsuccessful!",
					icon: "error",
					text: `Something went wrong. Please try again later!`
				})
			}
		})
	}

	
	const activate = (productId, productName) =>{

		fetch(`https://e-commerce-api-djx5.onrender.com/products/${productId}/activate`,{
			method: "PUT",
			headers:{
				"Content-Type": "application/json",
				"Authorization": `Bearer ${localStorage.getItem('token')}`
			},
			body: JSON.stringify({
				isActive: true
			})
		})
		.then(res => res.json())
		.then(data =>{


			if(data){
				Swal.fire({
					title: "Activate Succesful!",
					icon: "success",
					text: `${productName} is now active.`
				})
				getData();
			}
			else{
				Swal.fire({
					title: "Activate Unsuccessful!",
					icon: "error",
					text: `Something went wrong. Please try again later!`
				})
			}
		})
	}

	useEffect(()=>{

		getData();
	})
	return(
		(user.isAdmin)
		?
		<>
			<div className="mt-5 mb-3 text-center">
				<h1>Admin Dashboard</h1>
				<Button as={Link} to="/addProduct" variant="primary" size="lg" className="mx-2 my-2">Add Product</Button>
			</div>
			<Table bordered hover>
		     		<thead>
		       			<tr>
		         			
		         			<th>Product Name</th>
		         			<th>Description</th>
		         			<th>Price</th>
		         			<th>Status</th>
		         			<th>Actions</th>
		       			</tr>
		     		</thead>
		     					<tbody>
		       				{ allProducts }
		     					</tbody>
		   	</Table>
		</>
		:
		<Navigate to="/products"/>
	)
}