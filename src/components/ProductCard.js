import { Link } from 'react-router-dom';
import { Card, Button, Col, Row } from 'react-bootstrap';


export default function ProductCard({productProp}){


	let { name, description, price, _id } = productProp;


	return(
		<Row>
		<Col>
		<div className="text-center">
		<Card className="h-100 shadow-sm bg-white rounded">
			<Card.Body className="product-card">
			 <Card.Img variant="top" src="https://www.logo.wine/a/logo/Ryzen/Ryzen-AMD-White-Dark-Background-Logo.wine.svg"  width="300" height="400" alt=""  />
				<Card.Title>{name}</Card.Title>
				<Card.Subtitle>Description:</Card.Subtitle>
				<Card.Text>{description}</Card.Text>
				<Card.Subtitle>Price:</Card.Subtitle>
				<Card.Text>PHP {price}</Card.Text>
				
				<Button id="details" as={Link} to={`/products/${_id}`}>Details</Button>
			</Card.Body>
		</Card>
		</div>
		</Col>
		</Row>
	)
}