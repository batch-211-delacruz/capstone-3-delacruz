import { useState, useEffect, useContext } from 'react';
import { Container, Card, Button, Row, Col, Form } from 'react-bootstrap';
import { useParams, useNavigate, Link } from 'react-router-dom';
import UserContext from '../UserContext';
import Swal from 'sweetalert2';


export default function ProductView(){

	
	const { user } = useContext(UserContext);
	const navigate = useNavigate();
	const { productId } = useParams();
	const [name, setName] = useState('');
	const [description, setDescription] = useState('');
	const [price, setPrice] = useState(0);
	const [count, setCount] = useState(0);

  function inc() {
    setCount(function (prevCount) {
      return (prevCount += 1);
    });
  }

  function dec() {
    setCount(function (prevCount) {
      if (prevCount > 0) {
        return (prevCount -= 1); 
      } else {
        return (prevCount = 0);
      }
    });
  }



	
	const checkoutProduct = (productId) =>{


		fetch(`https://e-commerce-api-djx5.onrender.com/users/checkout/${productId}`,{
			method:'POST',
			headers:{
				'Content-Type':'application/json',
				"Authorization":`Bearer ${localStorage.getItem('token')}`
			},
			body: JSON.stringify({
				name: name,
				price: price,
				productId: productId,
				user: user.id
				
			})
		})
		.then(res=>res.json())
		.then(data=>{
		console.log(data)

			if(data===true){

				Swal.fire({
				  title: "Order Successfully created",
				  width: 600,
				  padding: '3rem',
				  background: 'url("https://wallpaperaccess.com/full/1285952.jpg")',
				  icon: "success",
				  text: `Thank you for buying ${name}!`
				});
				
				navigate("/products")

			}else{

				Swal.fire({
				  title: "Something went wrong!",
				  icon: "error",
				  text: "Check your credentials!"
				});

			}

		})
	}

	useEffect(()=>{
	
		
		fetch(`https://e-commerce-api-djx5.onrender.com/products/${productId}`)
		.then(res=>res.json())
		.then(data=>{
			
			setName(data.name);
			setDescription(data.description);
			setPrice(data.price);
		})

	},[productId])

	return(
		<Container className="mt-5">
		  <Row>
		     <Col lg={{span:6, offset:3}}>
				<Card>
					<Card.Body>
						<Card.Title>{name}</Card.Title>
						<Card.Subtitle>Description:</Card.Subtitle>
						<Card.Text>{description}</Card.Text>
						<Card.Subtitle>Price:</Card.Subtitle>
						<Card.Text>PHP {price}</Card.Text>
						<Card.Text>Quantity</Card.Text>
					
						<Form>
						<Button id="add" onClick={inc}>+</Button>
						
						<textarea id="quant"rows="1" cols="10"
			                value = {count}
			                onChange={e => dec(e.target.value)}
			                required >{count}
			      </textarea>
			      <Button id="minus" onClick={dec}>-</Button>
			      </Form>
						
						<br></br>


						{
							(user.id!==null)?

							<Button type="submit" id="submitBtn" variant="primary" onClick={()=>checkoutProduct(productId)}>Place Order</Button>
							:
							<Link className="btn btn-danger" to="/login">Log in to Buy Products</Link>
						}
						
					</Card.Body>
				</Card>
		     </Col>
		  </Row>
		</Container>
	)
}